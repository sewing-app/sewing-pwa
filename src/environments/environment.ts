// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDqvvEV7MkjfJH6lpwCLEOps0x9OAyHiIc",
    authDomain: "sewingapp-14a4c.firebaseapp.com",
    databaseURL: "https://sewingapp-14a4c.firebaseio.com",
    projectId: "sewingapp-14a4c",
    storageBucket: "sewingapp-14a4c.appspot.com",
    messagingSenderId: "443324360713",
    appId: "1:443324360713:web:4c601e07cd3018ee65b0d8",
    measurementId: "G-W932VV9NZF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
