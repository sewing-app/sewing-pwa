import { IProjectsService } from 'src/interfaces/services/projects/IProjectsService';
import { Injectable } from '@angular/core';
import { Project } from 'src/models/entities/projects/Project';
import { DeviceService } from '../device/DeviceService';
import { ProjectsRepository } from 'src/repositories/projects/ProjectsRepository';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService implements IProjectsService
{
  constructor(private _deviceService: DeviceService,
    private _projectsRepository: ProjectsRepository) { }

  public async createNewProject(project: Project): Promise<string>
  {
    project.user_id = this._deviceService.getLoggedInUserId();
    return await this._projectsRepository.insert(project);
  }

}
