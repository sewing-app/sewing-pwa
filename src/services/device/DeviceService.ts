import { IDeviceService } from 'src/interfaces/services/device/IDeviceService';
import { LocalStorageKeyType } from 'src/models/enumerations/device/LocalStorageKeyType';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeviceService implements IDeviceService
{
  public setLoggedInUser(userId: string, rememberMe: boolean): void
  {
    localStorage.setItem(LocalStorageKeyType.CURRENT_USER_ID, userId);
    localStorage.setItem(LocalStorageKeyType.KEEP_LOGGED_IN, (rememberMe || this.isInstalled()) ? "true" : "false");
  }
  public clearLoggedInUser(): void
  {
    localStorage.removeItem(LocalStorageKeyType.CURRENT_USER_ID);
    localStorage.removeItem(LocalStorageKeyType.KEEP_LOGGED_IN);
  }
  public getLoggedInUserId(): string
  {
    return localStorage.getItem(LocalStorageKeyType.CURRENT_USER_ID);
  }
  public isUserKeptLoggedIn(): boolean
  {
    return JSON.parse(localStorage.getItem(LocalStorageKeyType.KEEP_LOGGED_IN));
  }
  public isAndroidDevice(): boolean
  {
    let userAgent = navigator.userAgent || navigator.vendor;
    return /android/i.test(userAgent);
  }
  public isIosDevice(): boolean
  {
    let userAgent = navigator.userAgent || navigator.vendor;
    return /iPad|iPhone|iPod/i.test(userAgent);
  }
  public isMobile(): boolean
  {
    return window.innerWidth <= 425;
  }
  public isTablet(): boolean
  {
    return window.innerWidth > 425 && window.innerWidth <= 768;
  }
  public isDesktop(): boolean
  {
    return window.innerWidth > 768;
  }
  public isInstalled(): boolean
  {
    //TODO: Not totally right
    return window.matchMedia('(display-mode: standalone)').matches;
  }

}
