import { IRegistrationValidator } from 'src/interfaces/services/registrations/IRegistrationValidator';
import { ValidationErrors, AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { UsersRepository } from 'src/repositories/users/UsersRepository';
import { RegistrationValidatorMessages } from 'src/models/messages/registrations/RegistrationValidatorMessages';
import { isNullOrUndefined } from 'util';
import { NgModule } from '@angular/core';

@NgModule({
  providers: [UsersRepository]
})
export class RegistrationValidator implements IRegistrationValidator
{
  public static REPEATED_PASSWORD_ERROR = "passwordsDontMatch";
  public static USERNAME_ALREADY_EXISTS_ERROR = "usernameAlreadyExists";
  public static EMAIL_ALREADY_REGISTERED_ERROR = "emailAlreadyRegistered";

  constructor (private _usersRepository: UsersRepository) { }

  public getUsernameValidator(): AsyncValidatorFn
  {
    return async (control: AbstractControl): Promise<ValidationErrors | null> =>
    {
      let existingUserByUsername = await this._usersRepository.getByUsername(control.value);
      return existingUserByUsername == null
        ? null
        : { [RegistrationValidator.USERNAME_ALREADY_EXISTS_ERROR]: RegistrationValidatorMessages.USERNAME_ALREADY_EXISTS_ERROR_MESSAGE };
    }
  }

  public getEmailValidator(): AsyncValidatorFn
  {
    return async (control: AbstractControl): Promise<ValidationErrors | null> =>
    {
      let existingUserByEmail = await this._usersRepository.getByEmail(control.value);
      return existingUserByEmail == null
        ? null
        : { [RegistrationValidator.EMAIL_ALREADY_REGISTERED_ERROR]: RegistrationValidatorMessages.EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE };
    }
  }

  public getRepeatedPasswordValidationErrors(password: string, repeatedPassword: string): ValidationErrors
  {
    if (repeatedPassword.length > 0 && repeatedPassword != password)
    {
      return { [RegistrationValidator.REPEATED_PASSWORD_ERROR]: RegistrationValidatorMessages.PASSWORDS_DONT_MATCH_ERROR_MESSAGE };
    } else
    {
      return null;
    }
  }

}
