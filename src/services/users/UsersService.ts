import { IUsersService } from 'src/interfaces/services/users/IUsersService';
import { User } from 'src/models/entities/users/User';
import { UsersRepository } from 'src/repositories/users/UsersRepository';
import { Injectable } from '@angular/core';
import { LoginFormDataDto } from 'src/models/dtos/login/LoginFormDataDto';
import { DeviceService } from '../device/DeviceService';

@Injectable({
  providedIn: 'root'
})
export class UsersService implements IUsersService {

  constructor(private _usersRepository: UsersRepository,
    private _deviceService: DeviceService) { }

  public async registerNewUser(user: User): Promise<boolean>
  {
    if (await this.getRegisterNewUserValidation(user)) {
      let insertedUserId = await this._usersRepository.insert(user);
      return insertedUserId !== null && insertedUserId.length > 0;
    }
    return false;
  }

  public async login(loginData: LoginFormDataDto): Promise<boolean>
  {
    let user = await this._usersRepository.getByUsernameAndPassword(loginData.username, loginData.password);
    this._deviceService.setLoggedInUser(user.id, loginData.remember_me);
    return user !== null && user.id.length > 0;
  }

  private async getRegisterNewUserValidation(user: User): Promise<boolean>
  {
    let existingUserByEmail = await this._usersRepository.getByEmail(user.email);
    if (existingUserByEmail != null) {
      return false;
    }
    let existingUserByUserName = await this._usersRepository.getByUsername(user.username);
    if (existingUserByUserName != null) {
      return false;
    }
    return true
  }

}
