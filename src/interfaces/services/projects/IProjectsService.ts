import { Project } from 'src/models/entities/projects/Project';

export interface IProjectsService {
  createNewProject(project: Project): Promise<string>;

}
