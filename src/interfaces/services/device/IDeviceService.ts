export interface IDeviceService
{
  setLoggedInUser(userId: string, rememberMe: boolean): void;
  clearLoggedInUser(): void;
  getLoggedInUserId(): string;
  isUserKeptLoggedIn(): boolean;
  isAndroidDevice(): boolean;
  isIosDevice(): boolean;
  isMobile(): boolean;
  isTablet(): boolean;
  isDesktop(): boolean;
  isInstalled(): boolean;
}
