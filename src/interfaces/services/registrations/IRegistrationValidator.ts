import { ValidationErrors, AsyncValidatorFn } from '@angular/forms';

export interface IRegistrationValidator {

    getUsernameValidator(): AsyncValidatorFn;
    getEmailValidator(): AsyncValidatorFn;
    getRepeatedPasswordValidationErrors(password: string, repeatedPassword: string): ValidationErrors;
}
