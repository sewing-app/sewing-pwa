import { User } from 'src/models/entities/users/User';
import { LoginFormDataDto } from 'src/models/dtos/login/LoginFormDataDto';

export interface IUsersService {
  registerNewUser(user: User): Promise<boolean>;
  login(loginData: LoginFormDataDto): Promise<boolean>;
}
