import { IBaseRepository } from '../IBaseRepository';
import { Mannequin } from 'src/models/entities/mannequins/Mannequin';
import { SexType } from 'src/models/enumerations/mannequins/SexType';

export interface IMannequinsRepository extends IBaseRepository<Mannequin>
{
  getListBySexType(sexType: SexType): Promise<Mannequin[]>;
}
