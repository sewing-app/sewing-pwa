import { IBaseRepository } from '../IBaseRepository';
import { GarmentModel } from 'src/models/entities/projects/GarmentModel';
import { GarmentType } from 'src/models/enumerations/projects/GarmentType';

export interface IGarmentModelsRepository extends IBaseRepository<GarmentModel>
{
  getByGarmentType(garmentType: GarmentType): Promise<GarmentModel[]>;
}
