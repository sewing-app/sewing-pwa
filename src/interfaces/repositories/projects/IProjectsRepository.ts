import { IBaseRepository } from '../IBaseRepository';
import { Project } from 'src/models/entities/projects/Project';
import { ProjectStatusType } from 'src/models/enumerations/projects/ProjectStatusType';

export interface IProjectsRepository extends IBaseRepository<Project> {
  getListByUserId(userId: string, status?: ProjectStatusType): Promise<Project[]>;
  getListOfPublicProjects(): Promise<Project[]>;
}
