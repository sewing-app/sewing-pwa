export interface IBaseRepository<T extends Object> {
    getById(id: string): Promise<T>;
    getListByIds(ids: string[]): Promise<T[]>;
    update(entity: T): void;
    insert(entity: T): Promise<string>;
    delete(entity: T): void;
    deleteById(id: string): void;
    deleteList(entities: T[]): void;
    deleteByIds(ids: string[]): void;
    
}