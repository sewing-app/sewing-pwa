import { IBaseRepository } from '../IBaseRepository';
import { User } from 'src/models/entities/users/User';

export interface IUsersRepository extends IBaseRepository<User> {
    getByUsername(username: string): Promise<User>;
    getByEmail(email: string): Promise<User>;
    getByUsernameAndPassword(username: string, password: string): Promise<User>;
}
