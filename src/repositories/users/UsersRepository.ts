import { BaseRepository, FirestoreQueryClause } from '../BaseRepository';
import { User } from 'src/models/entities/users/User';
import { IUsersRepository } from 'src/interfaces/repositories/users/IUsersRepository';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersRepository extends BaseRepository<User> implements IUsersRepository {

    constructor(db: AngularFirestore) {
        super(db, new User());
    }

    getByUsername(username: string): Promise<User>
    {
      return this.getFirst(user => user.username, "==", username);
    }

    getByEmail(email: string): Promise<User>
    {
      return this.getFirst(user => user.email, "==", email);
    }

    getByUsernameAndPassword(username: string, password: string): Promise<User>
    {
      let clauses = [
        new FirestoreQueryClause<User>(user => user.username, "==", username),
        new FirestoreQueryClause<User>(user => user.password, "==", password)
      ];
      return this.getFirstComplex(clauses);
    }

}
