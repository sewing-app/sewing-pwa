import { BaseRepository, FirestoreQueryClause } from '../BaseRepository';
import { Project } from 'src/models/entities/projects/Project';
import { IProjectsRepository } from 'src/interfaces/repositories/projects/IProjectsRepository';
import { AngularFirestore } from '@angular/fire/firestore';
import { ShareConfigurationType } from 'src/models/enumerations/configuration/ShareConfigurationType';
import { Injectable } from '@angular/core';
import { ProjectStatusType } from 'src/models/enumerations/projects/ProjectStatusType';

@Injectable({
  providedIn: 'root'
})
export class ProjectsRepository extends BaseRepository<Project> implements IProjectsRepository
{
  constructor (db: AngularFirestore)
  {
    super(db, new Project());
  }

  getListByUserId(userId: string, status?: ProjectStatusType): Promise<Project[]>
  {
    if (status != null) {
      let clauses = [
        new FirestoreQueryClause<Project>(project => project.user_id, "==", userId),
        new FirestoreQueryClause<Project>(project => project.status, "==", status)
      ];
      return this.getListComplex(clauses);
    } else {
      return this.getList(project => project.user_id, "==", userId);
    }
  }

  getListOfPublicProjects(): Promise<Project[]>
  {
    return this.getList(project => project.share_configuration, "==", ShareConfigurationType.EVERYONE);
  }

}
