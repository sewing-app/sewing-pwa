import { BaseRepository } from '../BaseRepository';
import { GarmentModel } from 'src/models/entities/projects/GarmentModel';
import { IGarmentModelsRepository } from 'src/interfaces/repositories/projects/IGarmentModelsRepository';
import { AngularFirestore } from '@angular/fire/firestore';
import { GarmentType } from 'src/models/enumerations/projects/GarmentType';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GarmentModelsRepository extends BaseRepository<GarmentModel> implements IGarmentModelsRepository
{
  constructor (db: AngularFirestore)
  {
    super(db, new GarmentModel());
  }

  getByGarmentType(garmentType: GarmentType): Promise<GarmentModel[]>
  {
    return this.getList(garmentModel => garmentModel.garment_type, "==", garmentType);
  }

}
