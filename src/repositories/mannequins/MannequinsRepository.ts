import { BaseRepository } from '../BaseRepository';
import { Mannequin } from 'src/models/entities/mannequins/Mannequin';
import { IMannequinsRepository } from 'src/interfaces/repositories/mannequins/IMannequinsRepository';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { SexType } from 'src/models/enumerations/mannequins/SexType';

@Injectable({
  providedIn: 'root'
})
export class MannequinsRepository extends BaseRepository<Mannequin> implements IMannequinsRepository
{
  constructor (db: AngularFirestore)
  {
    super(db, new Mannequin());
  }

  getListBySexType(sexType: SexType): Promise<Mannequin[]>
  {
    return this.getList(mannequin => mannequin.sex_type, "==", sexType);
  }

}
