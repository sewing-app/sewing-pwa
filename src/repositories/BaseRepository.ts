import { IBaseRepository } from 'src/interfaces/repositories/IBaseRepository';
import { FirestoreSimple, Collection } from '@firestore-simple/web'
import { AngularFirestore, DocumentData } from '@angular/fire/firestore';
import { BaseEntity } from 'src/models/entities/BaseEntity';
import { nameof } from 'ts-simple-nameof';
import { QuerySnapshot, DocumentSnapshot } from '@angular/fire/firestore/angular-fire-firestore';
import { Query } from '@firestore-simple/web/dist/types';

export abstract class BaseRepository<T extends BaseEntity> implements IBaseRepository<T>
{

  private _firestore: FirestoreSimple;
  protected collection: Collection<T>;

  protected constructor (private db: AngularFirestore, entity: T)
  {
    this._firestore = new FirestoreSimple(db.firestore);
    this.collection = this._firestore.collection<T>({ path: entity._dbRoute });
  }

  public getById(id: string): Promise<T>
  {
    return this.collection.fetch(id);
  }

  public getListByIds(ids: string[]): Promise<T[]>
  {
    return this.getList(entity => entity.id, "in", ids);
  }

  public update(entity: T): void
  {
    this.getDocumentRefById(entity.id).then(documentSnapshot =>
    {
      documentSnapshot.ref.update(entity.getEntityData())
        .then(() => console.log(documentSnapshot.id + " updated"))
        .catch(err => console.log("Error trying to update document: " + err));
    });
  }

  public async insert(entity: T): Promise<string>
  {
    const addedDocument = await this.collection.collectionRef.add(entity.getEntityData());
    await addedDocument.update((<T>{ id: addedDocument.id }));
    console.log(addedDocument.id + " document added to " + this.collection.collectionRef.id);
    return addedDocument.id;
  }

  public delete(entity: T): void
  {
    this.deleteById(entity.id);
  }

  public deleteById(id: string): void
  {
    this.collection.delete(id);
  }

  public deleteList(entities: T[]): void
  {
    this.deleteByIds(entities.map(entity => entity.id));
  }

  public deleteByIds(ids: string[]): void
  {
    this.collection.bulkDelete(ids);
  }

  protected async getFirst(selector: (obj: T) => any, filterOption: firebase.firestore.WhereFilterOp, value: any): Promise<T>
  {
    const querySnapshot = await this.performSimpleQuery<T>(selector, filterOption, value);
    const entityDocs = await this.getEntityDocs(querySnapshot);
    return entityDocs.length > 0 ? entityDocs[0] : null;
  }

  protected async getFirstComplex(clauses: FirestoreQueryClause<T>[]): Promise<T>
  {
    const querySnapshot = await this.performComplexQuery(clauses);
    const entityDocs = await this.getEntityDocs(querySnapshot);
    return entityDocs.length > 0 ? entityDocs[0] : null;
  }

  protected async getList(selector: (obj: T) => any, filterOption: firebase.firestore.WhereFilterOp, value: any): Promise<T[]>
  {
    const querySnapshot = await this.performSimpleQuery<T>(selector, filterOption, value);
    return this.getEntityDocs(querySnapshot);
  }

  protected async getListComplex(clauses: FirestoreQueryClause<T>[]): Promise<T[]>
  {
    const querySnapshot = await this.performComplexQuery(clauses);
    return this.getEntityDocs(querySnapshot);
  }

  private async getEntityDocs(querySnapshot: QuerySnapshot<any>): Promise<T[]>
  {
    if (querySnapshot !== null && !querySnapshot.empty)
    {
      let entityDocSnapshots: DocumentSnapshot<T>[] = (<DocumentSnapshot<T>[]>querySnapshot.docs);
      return entityDocSnapshots.length > 0
        ? <T[]>entityDocSnapshots.map(entityDocSnapshot => entityDocSnapshot.data())
        : <T[]>[];
    }
    return <T[]>[];
  }

  private async getDocumentRefById(id: string): Promise<DocumentSnapshot<T>>
  {
    let querySnapshot = await this.performSimpleQuery<T>(entity => entity.id, "==", id);
    return querySnapshot.empty ? null : querySnapshot.docs[0];
  }

  private performSimpleQuery<T extends Object>(selector: (obj: T) => any, filterOption: firebase.firestore.WhereFilterOp, value: any): Promise<QuerySnapshot<any>>
  {
    return this.collection.collectionRef.where(nameof<T>(selector), filterOption, value).get();
  }

  private performComplexQuery(clauses: FirestoreQueryClause<T>[]): Promise<QuerySnapshot<any>>
  {
    let query: Query<DocumentData> = null;
    clauses.forEach(clause =>
    {
      if (query === null)
      {
        query = this.collection.collectionRef.where(nameof<T>(clause.selector), clause.filterOption, clause.value);
      }
      else
      {
        query.where(nameof<T>(clause.selector), clause.filterOption, clause.value);
      }
    });
    return query === null ? null : query.get();
  }
}

export class FirestoreQueryClause<T extends BaseEntity> {

  constructor (
    public selector: (obj: T) => any,
    public filterOption: firebase.firestore.WhereFilterOp,
    public value: any)
  {

  }
}
