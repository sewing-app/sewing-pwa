import { environment } from '../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './modules/material.module';
import { WelcomePageComponent } from './pages/welcome-page/welcome-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { BottomActionsBarComponent } from './components/bottom-actions-bar/bottom-actions-bar.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { WorkshopComponent } from './pages/home-page/workshop/workshop.component';
import { CatalogueComponent } from './pages/home-page/catalogue/catalogue.component';
import { InventoryComponent } from './pages/home-page/inventory/inventory.component';
import { BottomNavbarComponent } from './components/bottom-navbar/bottom-navbar.component';
import { TopTitleBarComponent } from './components/top-title-bar/top-title-bar.component';
import { TopTitleBarActionComponent } from './components/top-title-bar-action/top-title-bar-action.component';
import { ActiveProjectComponent } from './components/active-project/active-project.component';
import { ProjectPageComponent } from './pages/project-page/project-page.component';
import { NewProjectDialogComponent } from './components/new-project-dialog/new-project-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomePageComponent,
    LoginPageComponent,
    RegisterPageComponent,
    BottomActionsBarComponent,
    HomePageComponent,
    WorkshopComponent,
    CatalogueComponent,
    InventoryComponent,
    BottomNavbarComponent,
    TopTitleBarComponent,
    TopTitleBarActionComponent,
    ActiveProjectComponent,
    ProjectPageComponent,
    NewProjectDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
