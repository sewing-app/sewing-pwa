import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomActionsBarComponent } from './bottom-actions-bar.component';

describe('BottomActionsBarComponent', () => {
  let component: BottomActionsBarComponent;
  let fixture: ComponentFixture<BottomActionsBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomActionsBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomActionsBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
