import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopTitleBarActionComponent } from './top-title-bar-action.component';

describe('TopTitleBarActionComponent', () => {
  let component: TopTitleBarActionComponent;
  let fixture: ComponentFixture<TopTitleBarActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopTitleBarActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTitleBarActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
