import { Component, OnInit, Input, Output } from '@angular/core';
import { MatIcon } from '@angular/material/icon';

@Component({
  selector: 'app-top-title-bar-action',
  templateUrl: './top-title-bar-action.component.html',
  styleUrls: ['./top-title-bar-action.component.less']
})
export class TopTitleBarActionComponent implements OnInit {

  @Input() tooltip: string;
  @Input() icon: MatIcon;

  constructor() { }

  ngOnInit(): void {
  }

}
