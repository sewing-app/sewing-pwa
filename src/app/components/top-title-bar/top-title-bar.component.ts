import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-top-title-bar',
  templateUrl: './top-title-bar.component.html',
  styleUrls: ['./top-title-bar.component.less']
})
export class TopTitleBarComponent implements OnInit
{
  @Input() title: string;
  @Input() enableBackNavigation: boolean;
  @Output() onBackNavigationClicked = new EventEmitter();

  constructor (private _router: Router) { }

  ngOnInit(): void
  {
  }

  onBackButtonClicked(): void
  {
    this.onBackNavigationClicked.emit(undefined);
  }

  hasTitle(): boolean
  {
    return this.title != null && this.title != undefined && this.title.length > 0;
  }

}
