import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopTitleBarComponent } from './top-title-bar.component';

describe('TopTitleBarComponent', () => {
  let component: TopTitleBarComponent;
  let fixture: ComponentFixture<TopTitleBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopTitleBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTitleBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
