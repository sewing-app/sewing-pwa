import { Component, OnInit } from '@angular/core';
import { Project } from 'src/models/entities/projects/Project';
import { ProjectsService } from 'src/services/projects/ProjectsService';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { DialogResult } from 'src/models/dtos/dialogs/DialogResult';
import { DialogResultType } from 'src/models/enumerations/dialogs/DialogResultType';

@Component({
  selector: 'app-new-project-dialog',
  templateUrl: './new-project-dialog.component.html',
  styleUrls: ['./new-project-dialog.component.less']
})
export class NewProjectDialogComponent implements OnInit
{
  new_project = new Project();

  constructor (
    private _projectsService: ProjectsService,
    private _dialogRef: MatDialogRef<any>
  ) { }

  ngOnInit(): void
  {
  }

  saveAndClose(): void
  {
    this._projectsService.createNewProject(this.new_project)
      .then(projectId =>
      {
        if (projectId != null && projectId.length > 0)
        {
          this._dialogRef.close(new DialogResult(DialogResultType.SUCCESS, projectId));
        }
        else
        {
          this._dialogRef.close(new DialogResult(DialogResultType.ERROR));
        }
      })
      .catch(() => this._dialogRef.close(new DialogResult(DialogResultType.ERROR)));
  }

}
