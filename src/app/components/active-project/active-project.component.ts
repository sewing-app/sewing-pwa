import { Component, OnInit, Input, ElementRef, AfterViewInit } from '@angular/core';
import { ProjectInfoDto } from 'src/models/dtos/projects/ProjectInfoDto';

@Component({
  selector: 'app-active-project',
  templateUrl: './active-project.component.html',
  styleUrls: ['./active-project.component.less']
})
export class ActiveProjectComponent implements OnInit, AfterViewInit
{
  @Input() projectInfo: ProjectInfoDto;

  constructor (private _elRef: ElementRef) { }

  ngOnInit(): void
  {
  }

  ngAfterViewInit(): void
  {
    let thisElement: Element = this._elRef.nativeElement;
    thisElement.setAttribute("project_id", this.projectInfo.project_id);
  }

}
