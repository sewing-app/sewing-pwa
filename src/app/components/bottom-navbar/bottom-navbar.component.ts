import { Component, OnInit, Input, HostListener, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-bottom-navbar',
  templateUrl: './bottom-navbar.component.html',
  styleUrls: ['./bottom-navbar.component.less']
})
export class BottomNavbarComponent implements OnInit, AfterViewInit
{

  @Input() destinations: NavBarDestination[];

  constructor (private _router: Router) { }

  ngOnInit(): void
  {
    this._router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(this.onNavigationEnd.bind(this));
  }

  ngAfterViewInit(): void
  {
    this.setActiveDestinationByUrl(this._router.url);
  }

  @HostListener('click', ['$event']) onClick(event: Event)
  {
  }

  private onNavigationEnd(event: NavigationEnd)
  {
    this.setActiveDestinationByUrl(event.urlAfterRedirects);
  }

  private setActiveDestinationByUrl(url: string)
  {
    var currentDestination = this.destinations
      .find(destination => url.includes(this.getRelativePath(destination.path)));
    if (currentDestination != null)
    {
      this.setActiveDestination(document.getElementById(currentDestination.id));
    }
  }

  private setActiveDestination(element: Element)
  {
    if (element.closest(".bottom-navbar-destination") != null)
    {
      var destinationElements = document.querySelectorAll(".bottom-navbar-destination");
      destinationElements.forEach(element =>
      {
        element.classList.remove("active");
      });
      element.closest(".bottom-navbar-destination").classList.add("active");
    }
  }

  private getRelativePath(path: string): string
  {
    return path.replace(new RegExp("./"), "");
  }

}

export interface NavBarDestination
{
  id: string;
  icon: string;
  label: string;
  path: string;

}
