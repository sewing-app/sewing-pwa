import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { WelcomePageComponent } from './pages/welcome-page/welcome-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { WorkshopComponent } from './pages/home-page/workshop/workshop.component';
import { InventoryComponent } from './pages/home-page/inventory/inventory.component';
import { CatalogueComponent } from './pages/home-page/catalogue/catalogue.component';
import { ProjectPageComponent } from './pages/project-page/project-page.component';


const routes: Routes = [
  { path: '', component: WelcomePageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterPageComponent },
  {
    path: 'home', component: HomePageComponent, children: [
      { path: 'workshop', component: WorkshopComponent },
      { path: 'inventory', component: InventoryComponent },
      { path: 'catalogue', component: CatalogueComponent }
    ]
  },
  { path: 'project/:id', component: ProjectPageComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
