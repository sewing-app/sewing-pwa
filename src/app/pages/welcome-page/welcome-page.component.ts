import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageKeyType } from 'src/models/enumerations/device/LocalStorageKeyType';
import { DeviceService } from 'src/services/device/DeviceService';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.less']
})
export class WelcomePageComponent implements OnInit
{

  constructor (private _router: Router,
    private _deviceService: DeviceService)
  {
    let userLoggedIn = _deviceService.getLoggedInUserId();
    if (userLoggedIn != null && userLoggedIn.length > 0 && _deviceService.isUserKeptLoggedIn())
    {
      _router.navigate(["/home"]);
    }
  }

  ngOnInit(): void
  {
  }

  goToLogin(): void
  {
    this._router.navigate(["/login"]);
  }

  goToRegister(): void
  {
    this._router.navigate(["/register"]);
  }

}
