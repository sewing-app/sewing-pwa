import { Component, OnInit } from '@angular/core';
import { LoginFormDataDto } from 'src/models/dtos/login/LoginFormDataDto';
import { Router } from '@angular/router';
import { UsersService } from 'src/services/users/UsersService';
import { DeviceService } from 'src/services/device/DeviceService';

@Component({
  selector: 'app-login',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.less'],
  providers: [UsersService]
})
export class LoginPageComponent implements OnInit
{
  hidePasswd = true;

  errorLabelControl: HTMLElement;
  login_data: LoginFormDataDto;

  constructor (private _router: Router,
    private _deviceService: DeviceService,
    private _usersService: UsersService)
  {
    this.login_data = new LoginFormDataDto();
  }

  ngOnInit(): void
  {
    this.errorLabelControl = document.getElementById("error-label");
    this.formatViewport();
  }

  login(): void
  {
    this._usersService.login(this.login_data).then(loggedIn =>
    {
      if (loggedIn)
      {
        this._router.navigate(["/home"]);
      } else
      {
        this.errorLabelControl.textContent = "El usuario o contraseña no es correcto.";
        this.errorLabelControl.hidden = false;
      }
    });
  }

  goToWelcome(): void
  {
    this._router.navigate([""]);
  }

  private formatViewport(): void
  {
    if (this._deviceService.isInstalled())
    {
      document.getElementsByName("remember_me")[0].hidden = true;
    }
  }

}
