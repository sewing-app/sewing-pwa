import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectInfoDto } from 'src/models/dtos/projects/ProjectInfoDto';
import { Project } from 'src/models/entities/projects/Project';
import { Mannequin } from 'src/models/entities/mannequins/Mannequin';
import { GarmentModel } from 'src/models/entities/projects/GarmentModel';
import { ProjectsRepository } from 'src/repositories/projects/ProjectsRepository';
import { MannequinsRepository } from 'src/repositories/mannequins/MannequinsRepository';
import { GarmentModelsRepository } from 'src/repositories/projects/GarmentModelsRepository';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.less']
})
export class ProjectPageComponent implements OnInit
{

  project = new Project();
  mannequin = new Mannequin();
  garment_model = new GarmentModel();

  constructor (private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _projectsRepository: ProjectsRepository,
    private _mannequinsRepository: MannequinsRepository,
    private _garmentModelsRepository: GarmentModelsRepository)
  {
    this.loadPreobtainedProjectInfo();
  }

  ngOnInit(): void
  {
    this.loadProjectData();
  }

  loadPreobtainedProjectInfo()
  {
    let projectInfo = history.state.data;
    if (projectInfo instanceof ProjectInfoDto)
    {
      this.project.id = projectInfo.project_id;
      this.project.name = projectInfo.name;
      this.project.description = projectInfo.description;
      this.project.mannequin_id = projectInfo.mannequin_id;
      this.project.garment_model_id = projectInfo.garment_model_id;
      this.mannequin.name = projectInfo.mannequin_name;
      this.garment_model.name = projectInfo.garment_model_name;
    }
  }

  async loadProjectData()
  {
    let projectId: string = this._activatedRoute.snapshot.params.id;
    this.project = await this._projectsRepository.getById(projectId);
    if (this.project != null)
    {
      this.mannequin = await this._mannequinsRepository.getById(this.project.mannequin_id);
      this.garment_model = await this._garmentModelsRepository.getById(this.project.garment_model_id);
    } else
    {
      this.goToWorkshop();
    }
  }

  goToWorkshop(): void
  {
    this._router.navigateByUrl("/home/workshop");
  }

}
