import { Component, OnInit } from '@angular/core';
import { NavBarDestination } from 'src/app/components/bottom-navbar/bottom-navbar.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.less']
})
export class HomePageComponent implements OnInit
{

  bottomBarDestinations: NavBarDestination[] = [
    { id: "nav-catalogue", icon: "import_contacts", label: "Catálogo", path: "./catalogue" },
    { id: "nav-workshop", icon: "content_cut", label: "Taller", path: "./workshop" },
    { id: "nav-inventory", icon: "inbox", label: "Inventario", path: "./inventory" }
  ]

  constructor (private _router: Router, private _currentRoute: ActivatedRoute) { }

  ngOnInit(): void
  {
    if (this._router.url == "/home")
    {
      var initialNavbarDestination = this.bottomBarDestinations.find(destination => destination.id == "nav-workshop");
      if (initialNavbarDestination != null)
      {
        this._router.navigate([initialNavbarDestination.path], { relativeTo: this._currentRoute });
      }
    }
  }

}
