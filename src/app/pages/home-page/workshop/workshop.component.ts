import { Component, OnInit } from '@angular/core';
import { ProjectInfoDto } from 'src/models/dtos/projects/ProjectInfoDto';
import { ProjectsRepository } from 'src/repositories/projects/ProjectsRepository';
import { ProjectStatusType } from 'src/models/enumerations/projects/ProjectStatusType';
import { Router } from '@angular/router';
import { MannequinsRepository } from 'src/repositories/mannequins/MannequinsRepository';
import { GarmentModelsRepository } from 'src/repositories/projects/GarmentModelsRepository';
import { MatDialog } from '@angular/material/dialog';
import { NewProjectDialogComponent } from 'src/app/components/new-project-dialog/new-project-dialog.component';
import { DialogResult } from 'src/models/dtos/dialogs/DialogResult';
import { DialogResultType } from 'src/models/enumerations/dialogs/DialogResultType';

@Component({
  selector: 'app-workshop',
  templateUrl: './workshop.component.html',
  styleUrls: ['./workshop.component.less']
})
export class WorkshopComponent implements OnInit
{
  active_projects_infos: Promise<ProjectInfoDto>[];

  constructor (private _router: Router,
    private _projectsRepository: ProjectsRepository,
    private _mannequinsRepository: MannequinsRepository,
    private _garmentModelsRepository: GarmentModelsRepository,
    private _dialog: MatDialog)
  {
    this.setActiveProjectsInfos();
  }

  ngOnInit(): void
  {
  }

  async setActiveProjectsInfos(): Promise<void>
  {
    let active_projects = await this._projectsRepository.getListByUserId(localStorage.getItem("current_user_id"), ProjectStatusType.ACTIVE);
    this.active_projects_infos = active_projects.map(async project => new ProjectInfoDto(
        project,
        await this._mannequinsRepository.getById(project.mannequin_id),
        await this._garmentModelsRepository.getById(project.garment_model_id)
      )
    );
  }

  onActiveProjectClick(event: Event): void
  {
    let clickedProject: Element = (event.target as Element).closest("app-active-project");
    if (clickedProject != null) {
      this._router.navigate([`/project/${clickedProject.attributes.getNamedItem("project_id").value}`]);
    }
  }

  onNewProjectClick(): void
  {
    this._dialog.open(NewProjectDialogComponent, {
      width: '100%', height: '100%', maxWidth: '100%', maxHeight: '100%'
    })
    .afterClosed().subscribe((result: DialogResult) => {
      if (result.result == DialogResultType.SUCCESS) {
        this._router.navigate([`/project/${result.data}`]);
      }
    });

  }

}
