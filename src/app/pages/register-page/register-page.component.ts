import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/models/entities/users/User';
import { FormControl, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { RegistrationValidator } from 'src/services/registrations/RegistrationValidator';
import { UsersService } from 'src/services/users/UsersService';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.less'],
  providers: [RegistrationValidator]
})
export class RegisterPageComponent implements OnInit
{
  usernameAlreadyExistsErrorKey = RegistrationValidator.USERNAME_ALREADY_EXISTS_ERROR;
  emailAlreadyRegisteredErrorKey = RegistrationValidator.EMAIL_ALREADY_REGISTERED_ERROR;
  repeatedPasswordErrorKey = RegistrationValidator.REPEATED_PASSWORD_ERROR;

  user: User;

  displayNameFormControl: FormControl;
  emailFormControl: FormControl;
  usernameFormControl: FormControl;
  repeatedPasswordFormControl: FormControl;
  passwordFormControl: FormControl;

  constructor (private _router: Router,
    private _registrationDiscreetValidator: RegistrationValidator,
    private _usersService: UsersService)
  {
    this.user = new User();
    this.displayNameFormControl = new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ]);
    this.emailFormControl = new FormControl('', [
      Validators.required,
      Validators.email
    ],
    [_registrationDiscreetValidator.getEmailValidator()]
    );
    this.usernameFormControl = new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ],
    [_registrationDiscreetValidator.getUsernameValidator()]
    );
    this.repeatedPasswordFormControl = new FormControl('', [
      Validators.required,
      this.validateRepeatedPassword.bind(this)
    ]);
    this.passwordFormControl = new FormControl('', [
      Validators.required,
      this.validatePassword.bind(this)
    ]);
  }

  ngOnInit(): void
  {
  }

  goToWelcome(): void
  {
    this._router.navigate([""]);
  }

  register(): void
  {
    this._usersService.registerNewUser(this.user).then(registered => {
      if (registered) {
        this._router.navigate(["/home"]);
      } else {
        this._router.navigate([""]);
      }
    });
  }

  validatePassword(control: AbstractControl): ValidationErrors
  {
    return this._registrationDiscreetValidator.getRepeatedPasswordValidationErrors(control.value, this.repeatedPasswordFormControl.value);
  }

  validateRepeatedPassword(control: AbstractControl): ValidationErrors
  {
    return this._registrationDiscreetValidator.getRepeatedPasswordValidationErrors(this.user.password, control.value);
  }

}
