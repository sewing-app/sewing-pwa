import { DialogResultType } from 'src/models/enumerations/dialogs/DialogResultType';

export class DialogResult
{
  constructor (
    public result: DialogResultType,
    public data?: any
  ) { }
}
