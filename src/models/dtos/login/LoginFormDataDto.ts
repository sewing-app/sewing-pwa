export class LoginFormDataDto {
    public username: string;
    public password: string;
    public remember_me: boolean;
}