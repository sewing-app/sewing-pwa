import { Project } from 'src/models/entities/projects/Project';
import { Mannequin } from 'src/models/entities/mannequins/Mannequin';
import { GarmentModel } from 'src/models/entities/projects/GarmentModel';

export class ProjectInfoDto {
  public project_id: string;
  public name: string;
  public description: string;
  public mannequin_id: string;
  public mannequin_name: string;
  public garment_model_id: string;
  public garment_model_name: string;

  constructor(project?: Project, mannequin?: Mannequin, garment_model?: GarmentModel)
  {
    if (project != null) {
      this.project_id = project.id;
      this.name = project.name;
      this.description = project.description;
      this.mannequin_id = project.mannequin_id;
      this.garment_model_id = project.garment_model_id;
    }
    if (mannequin != null) {
      this.mannequin_id = mannequin.id;
      this.mannequin_name = mannequin.name;
    }
    if (garment_model != null) {
      this.garment_model_id = garment_model.id;
      this.garment_model_name = garment_model.name;
    }
  }
}
