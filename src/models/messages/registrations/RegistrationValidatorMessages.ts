export class RegistrationValidatorMessages {

  public static PASSWORDS_DONT_MATCH_ERROR_MESSAGE: string = "Las contraseñas no coinciden";
  public static USERNAME_ALREADY_EXISTS_ERROR_MESSAGE: string = "El usuario ya existe";
  public static EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE: string = "Este email ya está registrado";

}
