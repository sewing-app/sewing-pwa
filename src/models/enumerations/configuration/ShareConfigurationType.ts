export enum ShareConfigurationType {
  EVERYONE = "EVERYONE",
  FRIENDS_ONLY = "FRIENDS_ONLY",
  LINK_ONLY = "LINK_ONLY",
  PRIVATE = "PRIVATE"
}
