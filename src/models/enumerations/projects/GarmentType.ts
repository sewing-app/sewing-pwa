export enum GarmentType {
  HEAD = "HEAD",
  TOP = "TOP",
  BOTTOM = "BOTTOM",
  FEET = "FEET",
  ACCESORY = "ACCESORY",
  OTHER = "OTHER"
}
