export enum ProjectStatusType {
  COMPLETED = "COMPLETED",
  ACTIVE = "ACTIVE",
  ARCHIVED = "ARCHIVED"
}
