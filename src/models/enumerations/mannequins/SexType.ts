export enum SexType {
  MALE = "MALE",
  FEMALE = "FEMALE",
  UNISEX = "UNISEX"
}
