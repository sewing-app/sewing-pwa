export enum LocalStorageKeyType
{
  CURRENT_USER_ID = "current_user_id",
  KEEP_LOGGED_IN = "keep_logged_in"
}
