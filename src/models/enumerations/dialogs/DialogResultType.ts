export enum DialogResultType {
  YES = "YES",
  NO = "NO",
  OK = "OK",
  CANCEL = "CANCEL",
  SUCCESS = "SUCCESS",
  ERROR = "ERROR"
}
