import { BaseEntity } from 'src/models/entities/BaseEntity';

export class User extends BaseEntity {
    readonly _dbRoute: string = "/users";

    public display_name: string;
    public email: string;
    public username: string;
    public password: string;

}
