import { BaseEntity } from '../BaseEntity';
import { GarmentType } from 'src/models/enumerations/projects/GarmentType';

export class GarmentModel extends BaseEntity {
  readonly _dbRoute: string = "/garment_models";

  public name: string;
  public garment_type: GarmentType;

}
