import { BaseEntity } from 'src/models/entities/BaseEntity';
import { ShareConfigurationType } from 'src/models/enumerations/configuration/ShareConfigurationType';
import { ProjectStatusType } from 'src/models/enumerations/projects/ProjectStatusType';

export class Project extends BaseEntity {
    readonly _dbRoute: string = "/projects";

    public name: string;
    public description: string;
    public status: ProjectStatusType;
    public share_configuration: ShareConfigurationType;
    public user_id: string;
    public mannequin_id: string;
    public garment_model_id: string;
}
