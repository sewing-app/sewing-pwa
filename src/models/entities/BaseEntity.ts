import { Storable } from '@firestore-simple/web/dist/types';

export abstract class BaseEntity {
    public id: string;

    abstract readonly _dbRoute: string;

    getEntityData(): object {
        const result = {};
        Object.keys(this).filter(key => !key.startsWith('_'))
            .map(key => result[key] = this[key]);
        return result;
    }
}