import { BaseEntity } from '../BaseEntity';
import { SexType } from 'src/models/enumerations/mannequins/SexType';

export class Mannequin extends BaseEntity
{
  readonly _dbRoute: string = "/mannequins";

  public name: string;
  public sex_type: SexType;

}
